﻿using HospitalApp.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HospitalApp.DataAccess
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options): base(options) 
        {
            
        }
        public ApplicationDbContext()
        {
            
        }
        public DbSet<PatientModel> PatientModels { get; set; }
        public DbSet<DoctorModel> doctorModels { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PatientModel>().HasKey(x => x.Tc);
            modelBuilder.Entity<DoctorModel>().HasKey(x => x.Id);
        }
    }

}