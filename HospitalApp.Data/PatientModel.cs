﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HospitalApp.Data
{
    public class PatientModel
    {
        public int Tc { get; set; }
        public string FirstName { get; set;}
        public string LastName { get; set;}
        public string Adress {get; set;}
        public string MailAdress { get; set;}
        public string PhoneNumber { get; set;}
    }
}